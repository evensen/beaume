import 'package:flutter/material.dart';
import 'home.dart';
import 'package:beaume/interface_elements/res_tile.dart';


class Restaurant extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Winkels en restaurants"),
        centerTitle: true,
        backgroundColor: Colors.green,
        leading: MaterialButton(                                                  // dit is de 'back' button
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
            child: Icon(Icons.arrow_back, color: Colors.white,),
            onPressed: () {
              var router = MaterialPageRoute(
                  builder:(BuildContext context) => Home());                          // Zie tel_tile voor uitleg

              Navigator.of(context).push(router);
            }
        ),
        automaticallyImplyLeading: false,
      ),
        body: Container(
          color: Color(0xFFf0f0f0),
          child: Stack(
            children: <Widget>[

              Padding(                                                                // These are the list-tiles (zie act_tile.dart)
                padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
                child: ListView(
                  children: <Widget>[
                    // ActTile takes "title", "leading image", "Text", "Google link".
                    ResTile('Boulangerie Dumay', Icon(Icons.shopping_cart, size: 35.0, color: Colors.white,), Colors.blue, 'Bakkerij in Aubenton. Vers stokbrood, croissants en taarten (5 min. met de auto).', 'https://goo.gl/maps/HD9LEGum5JPiHo3u7'),
                    ResTile('La Table de Maya', Icon(Icons.restaurant, size: 35.0, color: Colors.white,), Colors.deepOrangeAccent, 'Lokaal Frans restaurant in Aubenton met keuzemenus voor een gemiddelde prijs (5 min. met de auto).', 'https://goo.gl/maps/AcSvxNdsxi9b4iT7A'),
                    ResTile('Proxi Service', Icon(Icons.shopping_cart, size: 35.0, color: Colors.white,), Colors.deepPurple, 'Klein supermarktje in Aubenton (5 min. met de auto).', 'https://goo.gl/maps/CSxMQtJb65qCVmXm9'),
                    ResTile('Auchan', Icon(Icons.shopping_cart, size: 35.0, color: Colors.white,), Colors.black45, 'Grote retail zaak in Hirson (electronica, witgoed, speelgoed, etc.). Open op zondag en op doordeweekse avonden tot half 9 (20 min. met de auto).', 'https://goo.gl/maps/3naKWQ5NaQatn4vQ8'),
                    ResTile('Lidl', Icon(Icons.shopping_cart, size: 35.0, color: Colors.white,), Colors.lightGreen, 'Lidl Hirson. Grotere goedkope supermarkt. Op zondag gesloten (20 min. met de auto)', 'https://goo.gl/maps/PkTEgNjJCCq8Xnnm6'),
                    ResTile('E. Leclerc', Icon(Icons.shopping_cart, size: 35.0, color: Colors.white,), Colors.brown,  'Grotere supermarkt in Hirson. Op zondag open in de ochtend (20 min. met de auto)', 'https://goo.gl/maps/6z3C6WkAEvqo4cBH8'),
                    ResTile('Aldi', Icon(Icons.shopping_cart, size: 35.0, color: Colors.white,), Colors.blueAccent, 'Grotere goedkope supermarkt in Hirson. Op zondag gesloten (20 min. met de auto)', 'https://goo.gl/maps/3LP4pRwpt4XoU44r5'),
                    ResTile('Crédit Agrigole (bank)', Icon(Icons.shopping_cart, size: 35.0, color: Colors.white,), Colors.red, 'Bank met 24u pinautomaat in Hirson. Indien defect, ga naar Crédit Mutuel (20 min. met de auto)', 'https://goo.gl/maps/WVuaxATBRva9zNHF6'),
                    ResTile("McDonald's", Icon(Icons.restaurant, size: 35.0, color: Colors.white,), Colors.indigo, "McDonald's in Hirson. Met een McDrive (20 min. met de auto)", "https://goo.gl/maps/1627n71mfGvwpJtB7"),


                    Padding(padding: EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),)

                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}
