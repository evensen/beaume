import 'package:flutter/material.dart';
import 'home.dart';
import 'package:beaume/interface_elements/act_tile.dart';


class Activiteit extends StatefulWidget {
  @override
  _ActiviteitState createState() => _ActiviteitState();
}

class _ActiviteitState extends State<Activiteit> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Activiteiten"),
        centerTitle: true,
        backgroundColor: Colors.green,
        leading: MaterialButton(                                                  // dit is de 'back' button
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
          child: Icon(Icons.arrow_back, color: Colors.white,),
          onPressed: () {
            var router = MaterialPageRoute(
              builder:(BuildContext context) => Home());                          // Zie tel_tile voor uitleg

              Navigator.of(context).push(router);
          }
        ),
        automaticallyImplyLeading: false,
      ),

      body: Container(
        color: Color(0xFFf0f0f0),
        child: Stack(
          children: <Widget>[

            Padding(                                                                // These are the list-tiles (zie act_tile.dart)
              padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
              child: ListView(
                children: <Widget>[
                  // ActTile takes "title", "leading image", "Text", "Google link".
                  ActTile('Étang de la Motte', 'images/meertje.png', 'Étang de la Motte is een zwemmeertje met een klein strand (20 min. met de auto).', 'https://goo.gl/maps/vFqagpyAvQDTGhf77'),
                  ActTile('TerraAltitude', 'images/terra.png', 'TerraAltitude is een activiteitenpark met ziplines, bungee en paintball (45 min. met de auto).', 'https://goo.gl/maps/Q75nxEzeFh5F7jqq7'),
                  ActTile('Chateau de Sedan', 'images/sedan.png', 'Château de Sedan is een van de grootste middeleeuwse kastelen in Europa (1 uur met de auto).', 'https://www.google.com/maps/place/Castle+of+Sedan/@49.702163,4.94923,15z/data=!4m5!3m4!1s0x0:0xfeab946475e35860!8m2!3d49.702163!4d4.94923'),
                  ActTile('Chimay Brouwerij', 'images/chimay.png', 'Een klein museum met bierproeverij en een boswandeling naar de oorspronkelijke abdij (30 min. met de auto)', 'https://goo.gl/maps/DxeaP9cMW8HUaLT2A'),
                  ActTile('Charleville-Mézières', 'images/charleville.png', 'De historische hoofdstad van de Ardennen gelegen tussen twee rivieren (50 min. met de auto)', 'https://www.google.com/maps/place/place+Ducale/@49.7735431,4.7208789,15z/data=!4m5!3m4!1s0x0:0xde802eec2fee7901!8m2!3d49.7735431!4d4.7208789'),
                  ActTile('Musée Guerre et Paix en Ardennes', 'images/guerre.png', 'Modern museum over laatste drie grote oorlogen in de Ardennen (40 min. met de auto)', 'https://www.google.com/maps/place/Mus%C3%A9e+Guerre+et+Paix/@49.6006423,4.4168258,15z/data=!4m5!3m4!1s0x0:0xcad88d5db9119cbd!8m2!3d49.6006423!4d4.4168258'),
                  ActTile('La Cascade de Blangy', 'images/cascade.png', 'Een wandeling van ca. 1 uur rondom een meertje met kleine watervallen (20 min. met de auto)', 'https://goo.gl/maps/cRZ2frSEjEkoEdANA'),
                  Padding(padding: EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),)

                ],
              ),
            ),
          ],
        ),
      )

    );
  }
}
