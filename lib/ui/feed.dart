import 'package:flutter/material.dart';
import 'home.dart';

class Feed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Livestream"),
        centerTitle: true,
        backgroundColor: Colors.green,
        leading: MaterialButton(                                                  // dit is de 'back' button
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
            child: Icon(Icons.arrow_back, color: Colors.white,),
            onPressed: () {
              var router = MaterialPageRoute(
                  builder:(BuildContext context) => Home());                          // Zie tel_tile voor uitleg

              Navigator.of(context).push(router);
            }
        ),
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0), // Dit is de afstand tussen de listtiles
        child: Container(
          decoration: BoxDecoration( // Dit is de manier om een boxshadow op een container te krijgen
            borderRadius: BorderRadius.all(Radius.circular(6.0)), // Borderradius van de listtiles
            color: Colors.white,
            border: Border.all(color: Color(0xFFd9d9d9)),
            boxShadow: [
              BoxShadow(
                color: Color(0xFFd9d9d9),     // Dit is hoe we Hex-colors toevoegen. De 0xFF staat voor 'full opacity'
                offset: new Offset(0.0, 2.0),
                blurRadius: 2.0,
                spreadRadius: 1.0
            )
          ],
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
                  child: Icon(Icons.videocam_off, size: 100.0, color: Colors.green,),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 15.0),
                  child: Text("Sorry, de livestream is momenteel nog niet beschikbaar.", style:
                    TextStyle(
                      fontSize: 20.0,
                      color: Colors.grey,
                    ),
                    textAlign: TextAlign.center,
                  ),
                )
              ],

            ),
          )
        ),
      )
    );
  }
}
