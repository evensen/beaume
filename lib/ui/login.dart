import 'package:flutter/material.dart';
import 'package:beaume/ui/home.dart';

String _login = 'fem';
String _password = 'woef';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
          body: Stack(
            children: <Widget>[
              Center(
                child: Image.asset('images/tuin.jpg',
                  width: 800.0,
                  height: 1200.0,
                  fit: BoxFit.cover,                                             // boxfit.cover doesn't distort the picture
                ),
              ),

              Center(
                child: Container(
                  width: 400,
                  height: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,             // spaceEvenly maakt dat deze over de container verspreid worden (Verticaal)
                    children: <Widget>[

                      Container(                                                 // De container bepaalt de breedte van de input fields
                        width: 260.0,
                        child: Material(
                          elevation: 20.0,
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: Colors.green,                                     // Background van de Material (achter de icon)
                          child: TextFormField(                                  // Eerste form field (naam)
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(                         // Opmaak van input field
                              hintText: "Naam",
                              icon: Padding(
                                padding: const EdgeInsets.fromLTRB(14.0, 0.0, 0.0, 0.0),
                                child: Icon(Icons.person_outline, color: Colors.white,),
                              ),
                              border: OutlineInputBorder(borderRadius: BorderRadius.only(bottomRight: Radius.circular(10.0), topRight: Radius.circular(10.0))),
                              fillColor: Colors.white,
                              filled: true,                                      // Default is transparant
                            ),
                            validator: (value) {
                              if (value.toLowerCase() != _login) {
                                return "Deze combinatie is incorrect. Probeer opnieuw.";
                              }
                              else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),

                      Container(                                                 // De container bepaalt de breedte van de input fields
                        width: 260.0,
                        child: Material(
                          elevation: 20.0,
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: Colors.green,                                     // Background van de Material (achter de icon)
                          child: TextFormField(                                  // Tweede form field (wachtwoord)
                            obscureText: true,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(                         // Opmaak van input field
                              hintText: "Wachtwoord",
                              icon: Padding(
                                padding: const EdgeInsets.fromLTRB(14.0, 0.0, 0.0, 0.0),
                                child: Icon(Icons.lock_outline, color: Colors.white,),
                              ),
                              border: OutlineInputBorder(borderRadius: BorderRadius.only(bottomRight: Radius.circular(10.0), topRight: Radius.circular(10.0))),
                              fillColor: Colors.white,
                              filled: true,                                      // Default is transparant
                            ),
                            validator: (value) {
                              if (value.toLowerCase() != _password) {
                                return "Deze gebruikersnaam bestaat niet";
                              }
                              else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),

                      Container(
                        width: 150.0,
                        height: 50.0,
                        child: RaisedButton(
                          child: Text('Login', style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            )
                          ),
                          color: Colors.green,
                          elevation: 20.0,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),       // Met shape kunnen we border radius toevoegen
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              var router = MaterialPageRoute(
                                  builder:(BuildContext context) => Home());

                              Navigator.of(context).push(router);
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }
}
