import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http; // deze is nodig voor de http request van de api
import 'dart:convert'; // dit is nodig om de map in de http request te decoden
import 'package:beaume/util/utils.dart' as utils;
import 'package:beaume/interface_elements/sidemenu.dart';



// home class
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();   // Key voor side-menu knop

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,                                                          // Key voor side-menu knop
      drawer: SideMenu(),                                                   // Side-menu zit in ui/sidemenu!
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset('images/sat4.jpg',
              width: 800.0,
              height: 1200.0,
              fit: BoxFit.cover,
            ),
          ),

          Container(                                                      // In deze container komt de weather-data van de API
            alignment: Alignment.topLeft,
            margin: const EdgeInsets.fromLTRB(35.0, 140.0, 0.0, 0.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(2.0),
                  decoration: new BoxDecoration(
                    color: Colors.white, // border color
                    shape: BoxShape.circle),
                  height: 130.0,
                  width: 130.0,
                  child: CircleAvatar(
                    backgroundImage: AssetImage('images/huis.jpeg'),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(12.0, 0.0, 0.0, 0.0),
                    child: updateTempWidget(),
                  ),
                ),
              ],
            )
          ),

          Container(
            alignment: Alignment.bottomRight,
            padding: EdgeInsets.fromLTRB(0.0, 0.0, 25.0, 40.0),
            child: FloatingActionButton.extended(
              label: Text("Menu", style: TextStyle(
                fontSize: 25.0,
              ),),
              icon: Icon(Icons.menu),
              backgroundColor: Colors.green,
              onPressed: () => _scaffoldKey.currentState.openDrawer(),
            ),
          )
        ],
      )
    );
  }
}



// creeer future method voor het binnenhalen van de API payload
Future<Map> getWeather(String appId) async { // appId is gedefinieerd in utils. Async is nodig voor het ophalen van API payload
  String apiUrl = 'http://api.openweathermap.org/data/2.5/weather?lat=49.839745&lon=4.152260&appid=${utils.appId}&units=metric'; // hier passen we de city en de app id uit utils

  http.Response response = await http.get(apiUrl); // 'get' de response en haal de API payload binnen

  return json.decode(response.body); // return en decode de response body (api payload) in een map-format, waar we doorheen kunnen loopen
}

// genereer een nieuwe widget class met een future builder om makkelijker met de http request data om te kunnen gaan. Deze class hoort bij het bovenstaande.
// De futurebuilder class maakt dat we widgets kunnen bouwen die de data gebruiken uit een future class (in dit geval de API call)
Widget updateTempWidget() {
  return new FutureBuilder(                                             // the future builder allows us to get data from a future type and then build it.
      future: getWeather(utils.appId),                            // this is the data we are getting (see above)
      // ignore: missing_return
      builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {     // here we build a 'snapshot' of that data (of type 'map'
        //here we get all the JSON data, setup the widget etc.
        if (snapshot.hasData) {
          Map content = snapshot.data;                                  // 'content' bevat nu alle data uit de payload (een 'snapshot')


          // Hiermee slaan extracten we de 'weather' items.
          List weatherList = content['weather']; // sla de list op
          String weatherNow = ""; // lege variable
          String weerNu = "";

          for (var items in weatherList){ //iterate over the list
            Map myMap = items; //store each map
            weatherNow = myMap['main']; // voeg 'main' item onder weather toe aan 'Weathernow'
          }

          // Vertaling weersomstandigheden
          if(weatherNow == 'Clouds')
            weerNu = 'Bewolkt';
          else if(weatherNow == 'Clear')
            weerNu = 'Zon';
          else if(weatherNow == 'Snow')
            weerNu = 'Sneeuw';
          else if(weatherNow == 'Rain')
            weerNu = 'Regen';
          else if(weatherNow == 'Drizzle')
            weerNu = 'Motregen';
          else if(weatherNow == 'Thunderstorm')
            weerNu = 'Onweer';
          else if(weatherNow == 'Fog')
            weerNu = 'Mist';

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,               // allignment naar links
            children: <Widget>[

              Padding(padding: EdgeInsets.all(10.0),),

              Row(
                children: <Widget>[
                  Text("Weer: ", style: tempStyle_2(),),
                  Text(weerNu,            // zie hierboven
                      style: tempStyle()),
                ],
              ),

              Row(
                children: <Widget>[
                  Text("Temperatuur: ", style: tempStyle_2(),),
                  Text("${content['main']['temp'].toString()} ℃",            // hier navigeren we naar de juiste data in de API payload (en converteren we double naar een string)
                      style: tempStyle()),
                ],
              ),
              Row(
                children: <Widget>[
                  Text("Luchtvochtigheid: ", style: tempStyle_2()),
                  Text("${content['main']['humidity'].toString()}%",            // hier navigeren we naar de juiste data in de API payload (en converteren we double naar een string)
                      style: tempStyle()),
                ],
              ),
              Row(
                children: <Widget>[
                  Text("Windsnelheid: ", style: tempStyle_2(),),
                  Text("${content['wind']['speed'].toString()} km/u",            // hier navigeren we naar de juiste data in de API payload (en converteren we double naar een string)
                      style: tempStyle()),
                ],
              ),
            ],
          );
        } else return Container();                                        // dit voorkomt foutmeldinge over het ontbreken van return statement
      });
}


// Textstyle voor temperatuur voor gebruik in de main
TextStyle tempStyle() {
  return TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontStyle: FontStyle.normal,
      fontSize: 15.0
  );
}

TextStyle tempStyle_2() {
  return TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.normal,
      fontSize: 15.0
  );
}