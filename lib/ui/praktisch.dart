import 'package:flutter/material.dart';
import 'package:beaume/interface_elements/prac_tile2.dart';
import 'package:beaume/ui/home.dart';

class Praktisch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Praktische zaken"),
        centerTitle: true,
        backgroundColor: Colors.green,
        leading: MaterialButton(                                                  // dit is de 'back' button
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
            child: Icon(Icons.arrow_back, color: Colors.white,),
            onPressed: () {
              var router = MaterialPageRoute(
                  builder:(BuildContext context) => Home());                          // Zie tel_tile voor uitleg

              Navigator.of(context).push(router);
            }
        ),
        automaticallyImplyLeading: false,
      ),
      body: Container(
        color: Color(0xFFf0f0f0),
        child: Stack(
          children: <Widget>[
            Padding(padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
            child: ListView(
              children: <Widget>[

                // dit zijn de tiles. Voor content van de text, zie hieronder
                PracTile("Poort", Colors.teal, Icon(Icons.directions_car), "De poort aan de straat is losjes 'gesloten' met een touwtje."),
                PracTile("Keukendeur", Colors.orangeAccent, Icon(Icons.lock), "Men komt het huisje binnen via de keukendeur. Het rolluik is vergrendeld met twee slotjes. De sleutel van de keukendeur moet tweemaal worden omgedraaid (niet te diep in het slot steken)."),
                PracTile("Sleutels", Colors.black45, Icon(Icons.vpn_key), "Binnen zijn alle sleutels te vinden in het sleutelkastje in de gang naar de garage."),
                PracTile("Luiken", Colors.brown, Icon(Icons.image), "Alle luiken moeten van binnenuit worden ontgrendeld / vergrendeld. Let op bij de beide openslaande deuren. Hier eerst voorzichtig de rechter deur ‘optillen’ (met hendel naast deur) en dan openen. Vervolgens de linker. Doe dit bij afsluiten in omgekeerde volgorde. Alleen van het raam bij de kachel worden de luiken van buiten geopend en weer vastgezet (middels een spie)."),
                PracTile("Water", Colors.blue, Icon(Icons.local_drink), "Het huisje is aangesloten op het waternet. Het water is veilig en drinkbaar (soms een beetje chloorsmaak). Om stromend water te krijgen moet op twee plaatsen de kraan worden opengezet. Bij de kleine put (plastic deksel) net buiten de poort bij de straat (de blauwe knop) en in het keukenkastje onder het aanrecht (rode hendel). Beide kranen een kwartslag draaien en bij vertrek weer sluiten. In de winter bij vertrek de kranen a.u.b. open zetten."),
                PracTile("Afvoer", Colors.indigo, Icon(Icons.wc), "Alle afvoer komt uit in een septic tank. Om die reden graag voorzichtig omgaan met agressieve stoffen en al te grote hoeveelheden water (kort douchen!)"),
                PracTile("Electriciteit", Colors.yellow, Icon(Icons.flash_on), "De electriciteit is continu aangesloten. Bij losse aparatuur (inclusief koelkast) stekkers uit het stopcontact. Boiler aanzetten door op slaapkamer de (gemarkeerde) schakelaar om te zetten. Zet deze bij vertrek ook weer uit."),
                PracTile("Gasfornuis", Colors.red, Icon(Icons.restaurant), "Er wordt gekookt op propaangas. In de (afgesloten) kist, buiten onder het keukenraam staan twee flessen. De aangesloten fles moet worden opgengedraaid (en gesloten bij vertrek). Wanneer de fles leeg is kan de reservefles worden aangesloten. De lege fles kan worden omgeruild voor een volle bij de kruidenier in Aubenton."),
                PracTile("Afval", Colors.black87, Icon(Icons.delete_sweep), "De blauwe afvalcontainer is bedoeld voor glas en petflessen. Het overige afval dient in plastic zakken te worden aangeboden. Dit alles mag pas maandagochtend aan de straat worden gezet. In overleg met de buurman kan het op zondag, op eigen terrein, vlak bij de poort worden weggezet. Hij zet het dan op maandag aan de straat. Anders afval graag meenemen naar huis."),
                PracTile("Château", Colors.greenAccent, Icon(Icons.location_city), "Het 'Château' is bouwvallig! Het is veilig als opslagplaats maar niet geschikt om te beklimmen. De kelder is afgesloten met een hangslot."),
                PracTile("Kampvuur", Colors.deepOrangeAccent, Icon(Icons.not_interested), "Het maken van grote open vuren is verboden èn (zeker in de zomer) onverstandig. Een klein vuurtje in de metalen schaal is geen probleem."),
                PracTile("Buren", Colors.deepPurple, Icon(Icons.lock), "De buren heten Jean-Francois en Anne-Lise Mongeny. Zij zullen het waarderen wanneer je even kennis maakt en verteld hoe lang je blijft. Je kunt hen ook om raad vragen bi problemen (zie telefoonboek)"),
                PracTile("Boodschappen", Colors.lightBlue, Icon(Icons.shopping_cart), "In Aubenton (4km) is een restaurant, bakker en een kleine supermarkt. In Hirson (12 km) zijn meerdere grote supermarkten (zie 'Activiteiten en locaties' voor routebeschrijvingen"),
                Padding(padding: EdgeInsets.fromLTRB(0.0, 250.0, 0.0, 0.0),)

              ],
            ),),
          ],
        ),
      ),
    );
  }
}

