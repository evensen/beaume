import 'package:flutter/material.dart';
import 'package:beaume/interface_elements/sidemenu.dart';
import 'package:beaume/ui/home.dart';
import 'package:beaume/interface_elements/tel_tile.dart';


class Telefoon extends StatefulWidget {
  @override
  _TelefoonState createState() => _TelefoonState();
}

class _TelefoonState extends State<Telefoon> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Telefoonboek"),
        centerTitle: true,
        backgroundColor: Colors.green,
        leading: MaterialButton(                                                  // dit is de 'back' button
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
            child: Icon(Icons.arrow_back, color: Colors.white,),
            onPressed: () {

              var router = MaterialPageRoute(
                  builder:(BuildContext context) => Home());

              Navigator.of(context).push(router);
            }
        ),
        automaticallyImplyLeading: false,
      ),
      drawer: SideMenu(),
      body: Container(
        color: Color(0xFFf0f0f0),                                                   // Dit is de background-color
        child: Stack(
          children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(15.0),
              child: ListView(
                children: <Widget>[
                  TelTile("0031614467346", "B", "Buren", Colors.green),             // Eerste telefoon-contact. Zie: interface elements.tel_tile
                  TelTile("0033323587917", "P", "Politiebureau Hirson", Colors.red),
                  TelTile("0033323588282", "H", "Ziekenhuis Hirson", Colors.blueAccent),

                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
