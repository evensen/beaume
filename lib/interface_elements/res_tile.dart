import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

// This tile is used on the activiteiten page



class ResTile extends StatelessWidget {
  ResTile(this._resTitle, this._resIcon, this._resColor, this._resText, this. _resMap);

  final String _resTitle;   // Title
  final Icon _resIcon;
  final String _resText;
  final String _resMap;
  final Color _resColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0), // Dit is de afstand tussen de listtiles
      child: Container(
          decoration: BoxDecoration( // Dit is de manier om een boxshadow op een container te krijgen
            borderRadius: BorderRadius.all(Radius.circular(6.0)), // Borderradius van de listtiles
            color: Colors.white,
            border: Border.all(color: Color(0xFFd9d9d9)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFd9d9d9),     // Dit is hoe we Hex-colors toevoegen. De 0xFF staat voor 'full opacity'
                  offset: new Offset(0.0, 2.0),
                  blurRadius: 2.0,
                  spreadRadius: 1.0
              )
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 10.0),
            child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),    // Padding between picture / text and buttons
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 0.0),
                            child: CircleAvatar(
                              radius: 30.0,
                              backgroundColor: _resColor,
                              child: _resIcon,
                            )
                          ),
                          Flexible(
                            child: Text(_resTitle,     // wordt ingevuld op Activiteiten view
                              style: TextStyle(
                                  fontSize: 24.0
                              ),),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Text(_resText, style: TextStyle(
                        fontSize: 18.0
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(5.0),),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        // Knop (kaart)
                        RaisedButton(                                                 // dit is de kaart-knop
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                          color: Colors.green,
                          elevation: 5.0,
                          onPressed: () async {
                            String url = _resMap;
                            if (await url_launcher.canLaunch(url)) {
                              await url_launcher.launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                          child: Container(
                            width: 70.0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(Icons.location_on, color: Colors.white,),
                                Text("Kaart", style: TextStyle(color: Colors.white),)
                              ],
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                )
            ),
          )
      ),
    );
  }
}
