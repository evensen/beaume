import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;



// This tile is used on the telefoonboek page

class TelTile extends StatelessWidget {
  TelTile(this._number, this._leading, this._name, this._color);

  final String _number;       // Phonenumber
  final String _leading;      // letter in the leading
  final String _name;         // Name of contact
  final Color _color;         // color of the leading


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),          // Dit is de afstand tussen de listtiles
      child: Container(
        decoration: BoxDecoration(                                      // Dit is de manier om een boxshadow op een container te krijgen
          borderRadius: BorderRadius.all(Radius.circular(6.0)),         // Borderradius van de listtiles
          color: Colors.white,
          border: Border.all(color: Color(0xFFd9d9d9)),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFd9d9d9),                                 // Dit is hoe we Hex-colors toevoegen. De 0xFF staat voor 'full opacity'
              offset: new Offset(0.0, 2.0),
              blurRadius: 2.0,
              spreadRadius: 1.0
            )
          ],

        ),
        child: ListTile(
          onTap: () => url_launcher.launch('tel:$_number'),        // Hiermee bellen we het nummer met de phone-app (url_launcher package noodzakelijk)
          leading: CircleAvatar(
            backgroundColor: _color,
            child: Text(_leading, style: TextStyle(                // dit is de leading
                fontSize: 20.0,
                color: Colors.white
            ),),
          ),
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
              child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(_name, style: TextStyle(                     // dit is de naam
                    fontSize: 19.0,
                    color: Color(0xFF3d3d3d)
                  ),),
                Icon(Icons.phone, color: Color(0xFF3d3d3d),)
              ],
            ),
          )
        ),
      ),
    );
  }
}
