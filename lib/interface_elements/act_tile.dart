import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

// This tile is used on the activiteiten page



class ActTile extends StatelessWidget {
  ActTile(this._actTitle, this._actImage, this._actText, this._actMap);

  final String _actTitle;   // Title
  final String _actImage;
  final String _actText;
  final String _actMap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0), // Dit is de afstand tussen de listtiles
      child: Container(
        decoration: BoxDecoration( // Dit is de manier om een boxshadow op een container te krijgen
          borderRadius: BorderRadius.all(Radius.circular(6.0)), // Borderradius van de listtiles
          color: Colors.white,
          border: Border.all(color: Color(0xFFd9d9d9)),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFd9d9d9),     // Dit is hoe we Hex-colors toevoegen. De 0xFF staat voor 'full opacity'
              offset: new Offset(0.0, 2.0),
              blurRadius: 2.0,
              spreadRadius: 1.0
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 10.0),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 15.0),
                  child: Text(_actTitle, style: TextStyle(             // title van het content block
                    fontSize: 24.0,
                    fontWeight: FontWeight.w600
                  ),),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),    // Padding between picture / text and buttons
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 0.0),
                        child: CircleAvatar(
                          backgroundImage: AssetImage(_actImage), // LET OP: Image.asselt werkt hier niet!,
                          radius: 40.0,
                        ),
                      ),
                      Flexible(
                        child: Text(_actText,     // wordt ingevuld op Activiteiten view
                          style: TextStyle(
                            fontSize: 18.0
                          ),),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[

                    // Knop (kaart)
                    RaisedButton(                                                 // dit is de kaart-knop
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      color: Colors.green,
                      elevation: 5.0,
                      onPressed: () async {
                          String url = _actMap;
                            if (await url_launcher.canLaunch(url)) {
                              await url_launcher.launch(url);
                            } else {
                              throw 'Could not launch $url';
                        }
                      },
                      child: Container(
                        width: 70.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.location_on, color: Colors.white,),
                            Text("Kaart", style: TextStyle(color: Colors.white),)
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )
          ),
        )
      ),
    );
  }
}


