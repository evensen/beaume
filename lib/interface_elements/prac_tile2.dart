import 'package:flutter/material.dart';

class PracTile extends StatelessWidget {
  PracTile(this._name, this._color, this._icon, this._prac_text);

  final String _name;         // Name on tile
  final Color _color;    // color of leading
  final Icon _icon;  // leading icon of the tile
  final String _prac_text; // text of the tile

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: Container(
        decoration: BoxDecoration(                                      // Dit is de manier om een boxshadow op een container te krijgen
          borderRadius: BorderRadius.all(Radius.circular(6.0)),         // Borderradius van de listtiles
          color: Colors.white,
          border: Border.all(color: Color(0xFFd9d9d9)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFd9d9d9),                                 // Dit is hoe we Hex-colors toevoegen. De 0xFF staat voor 'full opacity'
                offset: new Offset(0.0, 2.0),
                blurRadius: 2.0,
                spreadRadius: 1.0
            )
          ],
        ),
        child: ExpansionTile(
          trailing: CircleAvatar(  // hiermee overlappen we het lelijk 'trailing' peiltje
            backgroundColor: Colors.white,
          ),
          leading: CircleAvatar(
            backgroundColor: _color,
            child: _icon,
            foregroundColor: Colors.white,
          ),
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(_name, style: TextStyle(                     // dit is de naam
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF3d3d3d)
                ),),
              ],
            ),
          ),
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Text(
                    _prac_text,
                    style: TextStyle(
                        fontSize: 18.0,
                        color: Color(0xFF3d3d3d)
                    ),
                  ),
                )
              ],
            )
          ],),
      ),
    );
  }
}
