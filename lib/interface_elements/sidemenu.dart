import 'package:flutter/material.dart';
import 'package:beaume/ui/feed.dart';
import 'package:beaume/ui/praktisch.dart';
import 'package:beaume/ui/telefoon.dart';
import 'package:beaume/ui/activiteit.dart';
import 'package:beaume/ui/restaurant.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;


// In deze class zit het sidemenu

class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {

  @override
  Widget build(BuildContext context) {
    return Drawer(                                             // dit is de side menu
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
                color: Colors.green                               // Kleur van de header in het side-menu
            ),
            accountEmail: Text('fem@beaume.fr'),
            accountName: Text('Fem'),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.white,
              backgroundImage: AssetImage('images/fem.jpeg')    // LET OP: Image.asselt werkt hier niet!
            ),
          ),

          ListTile(
            leading: Icon(Icons.build),
            title: Text("Praktische zaken"),
            onTap: () {
              var routerPraktisch= MaterialPageRoute(
                  builder:(BuildContext context) => Praktisch());

              Navigator.of(context).push(routerPraktisch);
            },     // onTap met een anonymous method
          ),

          ListTile(
            leading: Icon(Icons.directions_bike),
            title: Text("Activiteiten"),
            onTap: () {
              var routerActiviteit = MaterialPageRoute(
                builder: (BuildContext context) => Activiteit()); // Hiermee maak je de 'tunnel'

              Navigator.of(context).push(routerActiviteit); // hiermee gebruik je de tunnel (navigeer je naar de pagina)
            },
          ),

          ListTile(
            leading: Icon(Icons.restaurant),
            title: Text("Winkels en restaurants"),
            onTap: () {
              var routerRestaurant= MaterialPageRoute(
                  builder: (BuildContext context) => Restaurant()); // Hiermee maak je de 'tunnel'

              Navigator.of(context).push(routerRestaurant); // hiermee gebruik je de tunnel (navigeer je naar de pagina)
            },
          ),

          ListTile(
            leading: Icon(Icons.videocam),
            title: Text("Livestream"),
            onTap: () {
              var routerFeed = MaterialPageRoute(
                  builder:(BuildContext context) => Feed());

              Navigator.of(context).push(routerFeed);
            },
          ),

          ListTile(
            leading: Icon(Icons.phone),
            title: Text("Telefoonboek"),
            onTap: () {
              var routerTelefoon = MaterialPageRoute(
                  builder:(BuildContext context) => Telefoon());

              Navigator.of(context).push(routerTelefoon);
            },
          ),

          Divider(), // Dit is een horizontaal lijntje

          Expanded(                                     // Met de Expanded + Alignment: FractionalOffset.bottomcenter schuiven we dit item naar beneden
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: ListTile(
                leading: Icon(Icons.calendar_today),
                title: Text("Kalender"),
                onTap: () => url_launcher.launch('http://calendar.google.com/calendar?cid=cWhlN2plaHNqZW45NHJ0ZDJlNW9hbHVnbWNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ'),
              ),
            ),
          ),
          Padding(padding: EdgeInsets.all(8.0),)
        ],
      ),
    );
  }
}

